const { reject } = require("./init_db")
const db = require("./init_db")


function checkTodoByID(id) {
    todoDB = db.get("todos").find({id: id})
    if(todoDB.value() == undefined) {
        throw new Error("This todo doesn't exist")
    }
}


const dbGetTodos = () => {
    return new Promise((resolve, reject) => {
        let todos = db.get("todos")
        resolve(todos)
    }) 
}

const dbGetTodo = (id) => {
    return new Promise((resolve, reject) => {
        try{
            checkTodoByID(id)
            let todo = db.get("todos").find({id : id}).value()
            resolve(todo)
        }catch(err){
            reject(err)
        }
    })
}

const dbAddTodo = (todo) => {
    return new Promise((resolve, reject) => {
        try{
            db.get("todos").push(todo).write()
            resolve()
        }catch(err){
            reject(err)
        }
    })
}


const dbUpdateTodo = (todo) => {
    return new Promise((resolve, reject) => {
        try{
            console.log(todo)
            console.log(todo["id"])
            checkTodoByID(todo["id"])
            db.get("todos").find({id: todo["id"]}).assign(todo).write()
            resolve()
        }catch(err){
            reject(err)
        }
    })
}

const dbDeleteTodo = (id) => {
    return new Promise((resolve, reject) => {
        try{
            checkTodoByID(id)
            db.get("todos").remove({id: id}).write()
            resolve()
        }catch(err){
            
            reject(err)
        }
    })
}


module.exports = {
    dbGetTodos,
    dbGetTodo,
    dbAddTodo,
    dbUpdateTodo,
    dbDeleteTodo
}