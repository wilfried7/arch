const express = require('express')
const {getTodo, getTodos, addTodo, updateTodo, deleteTodo} = require("../controllers/todos")




const todoRoute = express.Router()

todoRoute.get("", getTodos)

todoRoute.post("", addTodo)

todoRoute.patch('', updateTodo)

todoRoute.delete('', deleteTodo)

todoRoute.get('/:id', getTodo)

module.exports = todoRoute