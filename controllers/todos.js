const todoSchema = require("../../todo/todoSchema")
const {dbGetTodos, dbGetTodo, dbAddTodo, dbUpdateTodo, dbDeleteTodo} = require("../models/todos")
const {v4: uuidv4} = require("uuid")
var validate = require('jsonschema').validate;


function checkTodoSchema(todo) {
    let validSchema = validate(todo, todoSchema)
    console.log(validSchema.valid)
    if(!validSchema.valid){
        messages = []
        validSchema.errors.forEach((error) => {
          messages.push(error.message)
        })
        throw new Error(messages)
    }
}


const getTodos = async (req, res) => {
    await dbGetTodos()
        .then(todos => res.status(200).json(todos))
        .catch(err => res.status(400).json({"error":err.message}))
}

const getTodo = async (req, res) => {
    let id = req.params["id"]
    await dbGetTodo(id)
        .then(todo => res.status(200).json(todo))
        .catch(err => res.status(400).json({"error":err.message}))
}

const addTodo = async (req, res) => {
    try{
        let todo = req.body
        todo["id"] = uuidv4()
        checkTodoSchema(todo)
        await dbAddTodo(todo)
            .then(() => res.status(200).json({"message":"the new todo is added !"}))
            .catch((err) => res.status(400).json({"error":err.message}))
    }catch(err){
        res.status(400).json({"error":err.message})
    }
}


const updateTodo = async (req, res) => {
    try{
        let todo = req.body
        checkTodoSchema(todo)
        await dbUpdateTodo(todo)
            .then(() => res.status(200).json({"message":"The todo has been updated"}))
            .catch((err) => res.status(400).json({"error":err.message}))
    }catch(err){
        res.status(400).json({"error":err.message})
    }
}


const deleteTodo = async (req, res) => {
    try{
        let id = req.body["id"]
        await dbDeleteTodo(id)
            .then(() => res.status(200).json({"message":"The todo has been deleted"}))
            .catch((err) => res.status(400).json({"message":err.message}))
    }catch(err){
        res.status(400).json({"error":err.message})
    }

}

module.exports = {
    getTodo,
    getTodos,
    addTodo,
    updateTodo,
    deleteTodo
}