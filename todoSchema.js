const todoSchema = {
    "properties":{
        "id":{"type":"string"},
        "title":{"type":"string"},
        "dateBegin":{"type":"string", "format":"date"},
        "dateEnd":{"type":"string", "format":"date"},
        "statut":{"type":"string", "enum":["Non précisé", "En cours", "Achevée", "Annulée", "une tâche est requise"]},
        "tags":{"type": "array", "items": {"type": "string"}},
    },
    "required":["title", "statut", "tags","dateBegin","dateEnd"],
    "additionalProperties": false
}

module.exports = todoSchema;