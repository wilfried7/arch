const express = require("express")
const todoRoute = require("./routes/todos.js")


app = express()
app.use(express.json())
app.use("/api/todos", todoRoute)



app.listen(3000, () => {
    console.log("start")
})